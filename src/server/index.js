const csv=require('csvtojson');
const fileSystems = require('fs');
const ipl = require('./ipl')

const deliveriesCsvFilePath = '../data/deliveries.csv'
const matchesCsvFilePath = '../data/matches.csv'



// Number of matches played per year for all the years in IPL.

csv().fromFile(matchesCsvFilePath).then((matches) => {

    const result = ipl.matchesPerYear(matches);

    fileSystems.writeFileSync("../public/output/macthesPerYear.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });

});


// Number of matches won per team per year in IPL.

csv().fromFile(matchesCsvFilePath).then((matches) => {

    const result = ipl.matchesWonByTeamPerYear(matches)

    fileSystems.writeFileSync("../public/output/matchesWonByTeamPerYear.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });

});


// Extra runs conceded per team in the year 2016

csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {

    csv().fromFile(matchesCsvFilePath).then((matches) => {

        const result = ipl.extraRunsIn2016(matches, deliveries)

        fileSystems.writeFileSync("../public/output/extraRunIn2016.json", JSON.stringify(result), 'utf-8', (err) => {
            if(err) console.log(err);
        });

    });

})


// Top 10 economical bowlers in the year 2015

csv().fromFile(matchesCsvFilePath).then((matches) => {

    csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {

        const result = ipl.bestEconomyIn2015(matches, deliveries);
        
        fileSystems.writeFileSync("../public/output/bestEconomyIn2015.json", JSON.stringify(result), 'utf-8', (err) => {
            if(err) console.log(err);
        });

    });

});


// Find the number of times each team won the toss and also won the match

csv().fromFile(matchesCsvFilePath).then((matches) => {

    const result = ipl.teamsWonBothTossAndMatch(matches);

    fileSystems.writeFileSync("../public/output/teamsWonBothTossAndMatch.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });

});


// Find a player who has won the highest number of Player of the Match awards for each season

csv().fromFile(matchesCsvFilePath).then((matches) => {

    const result = ipl.mostManOfTheMatchAwardsPerSeason(matches);

    fileSystems.writeFileSync("../public/output/mostManOfTheMatchAwardsPerSeason.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });

});


// Find the bowler with the best economy in super overs

csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {

    const result = ipl.bestEconomicalBowlerInSuperOvers(deliveries);
    
    fileSystems.writeFileSync("../public/output/bestEconomicalBowlerInSuperOvers.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });
    
});


// Find the highest number of times one player has been dismissed by another player

csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {
    
    const result = ipl.batsmanDismissedMostlyByBowler(deliveries);

    fileSystems.writeFileSync("../public/output/batsmanDismissedMostlyByBowler.json", JSON.stringify(result), 'utf-8', (err) => {
        if(err) console.log(err);
    });

});


// Find the strike rate of a batsman for each season

csv().fromFile(deliveriesCsvFilePath).then((deliveries) => {

    csv().fromFile(matchesCsvFilePath).then((matches) => {

        const result = ipl.batterStrikeRatePerEachSeason(deliveries, matches, 'RG Sharma');

        fileSystems.writeFileSync("../public/output/batterStrikeRatePerEachSeason.json", JSON.stringify(result), 'utf-8', (err) => {
            if(err) console.log(err);
        });

    });

});