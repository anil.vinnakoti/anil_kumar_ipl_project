function matchesPerYear(matches) {
    const result = {};
    for (let match of matches) {
        const year = match['season'];
        
        if (result[year] === undefined) {
        result[year] = 1;
        } 
        else {
        result[year]++;
        };
    };
    return result;
};

function matchesWonByTeamPerYear(matches){
    const result = { };

    for(let match of matches){
        const year = match['season'];
        const win = match['winner'];
        
        if(result[year] === undefined){
            result[year] = {};

            if(result[year][win] === undefined){
                result[year][win] =1;
            }
            else{
                result[year][win] ++;
            }
        }
        else{

            if(result[year][win] === undefined){
                result[year][win] =1;
            }
            else{
                result[year][win] ++;
            }
        }
    }

    return  result;
};

function extraRunsIn2016(matches, deliveries) {
    const result = {};

        const matchIdArray2016 = [];
        for(let match of matches){
            if(match['season'] == 2016){
                matchIdArray2016.push(match['id']);
            };
        };

        const deliveriesIn2016 = [];
        for(let ball of deliveries){
            for(let id of matchIdArray2016){
                if(ball['match_id'] == id){
                    deliveriesIn2016.push(ball);
                };
            };
        };

        for(let ball of deliveriesIn2016){
            const extraRun = ball['extra_runs'];
            const bowlingTeam = ball['bowling_team'];

            if(result[bowlingTeam] === undefined){
                result[bowlingTeam] = parseInt(extraRun);
            }
            else {
                result[bowlingTeam] = result[bowlingTeam] + parseInt(extraRun);
            };
        };

    return result;
};

function bestEconomyIn2015(matches, deliveries){
    const stats = {};
    const matchIdArray2015 = [];

        for(let match of matches){

            if(match['season'] == 2015){

                matchIdArray2015.push(match['id']);
            };
        };

        const deliveriesIn2015 = [];
        for(let ball of deliveries){
            for(let id of matchIdArray2015){

                if(ball['match_id'] == id){

                    deliveriesIn2015.push(ball);
                };
            };
        };        

        for(let ball of deliveriesIn2015){
            const bowlerName = ball['bowler'];
            const runs = ball['total_runs'];

            if(stats[bowlerName] === undefined){

                stats[bowlerName] = { 'balls': 1,
                                      'runs' : 0 };

                if(stats[bowlerName]['runs'] === undefined){
                    stats[bowlerName]['runs'] = parseInt(runs);
                }
                else{
                    stats[bowlerName]['runs'] = stats[bowlerName]['runs'] + parseInt(runs);
                };
            }
            else {
                stats[bowlerName]['balls']++;

                if(stats[bowlerName]['runs'] === undefined){

                    stats[bowlerName]['runs'] = parseInt(runs);
                }
                else{
                    stats[bowlerName]['runs'] = stats[bowlerName]['runs'] + parseInt(runs);
                };
            };
        };

        for(let stat in stats){

            stats[stat]['economy'] = (stats[stat]['runs'] * 6)/ stats[stat]['balls'] ;
        }

        const entries = Object.entries(stats);

        const sorted = entries.sort((a, b) => a[1]['economy'] - b[1]['economy']);

        const bestEconomyArray = sorted.slice(0, 10);

        const result = Object.fromEntries(bestEconomyArray);

    return result;
};

function teamsWonBothTossAndMatch(matches) {
    const result = { };

    for(let match of matches){
        const tossWinner = match['toss_winner'];
        const matchWinner = match['winner'];

        if(result[tossWinner] == undefined){
            result[tossWinner] = 1;
        }
        else if(tossWinner == matchWinner){
            result[tossWinner]++;
        }
    }
    return result;
};

function mostManOfTheMatchAwardsPerSeason(matches) {
        const stats = { };
        const result = { };

        for(let match of matches){
        const manOfTheMatch = match['player_of_match'];
        const year = match['season'];

        if(!stats[year]){
            stats[year] = { };

            if(!stats[year][manOfTheMatch]){
                stats[year][manOfTheMatch] = 1;
            }
            else{
                stats[year][manOfTheMatch]++;

            };
        }
        else{

            if(!stats[year][manOfTheMatch]){
                stats[year][manOfTheMatch] = 1;
            }
            else{
                stats[year][manOfTheMatch]++;
            };
        };

    };

    const entries = Object.entries(stats);

    for(let item of entries){
        const obj = item[1];
        const year = item[0];

        let sorted = Object.entries(obj).sort( (a,b) => b[1] - a[1] );
        const top =sorted.slice(0,1);

        for(let awards of top){
            result[year] = awards;
        };
    };

    return result;
};

function bestEconomicalBowlerInSuperOvers(deliveries) {
    const stats = { };

    for(let ball of deliveries){
        const bowlerName = ball['bowler'];
        const runs = ball['total_runs'];

        if(ball['is_super_over'] != 0){

            if(stats[bowlerName] === undefined){
                stats[bowlerName] = { balls : 1,
                                        runs : 0 };
                                        
                stats[bowlerName]['runs'] += parseInt(runs);
            }else {

                stats[bowlerName]['balls']++;

                stats[bowlerName]['runs'] += parseInt(runs);
            };
        };
    };
    
    for(let stat in stats){

        stats[stat]['economy'] = (stats[stat]['runs'] * 6)/ stats[stat]['balls'] ;
    };

    const entries = Object.entries(stats);

    const sorted = entries.sort((a, b) => a[1]['economy'] - b[1]['economy']);

    const bestEconomyArray = sorted.slice(0, 1);

    const result = Object.fromEntries(bestEconomyArray);

    return result;
};

function batsmanDismissedMostlyByBowler(deliveries) {

    const stats = { };
    for(let ball of deliveries){
        const batsman = ball['player_dismissed'];
        const bowler = ball['bowler'];
        const dismissal = ball['dismissal_kind'];

        if(dismissal != 'run out' && dismissal != ''){

            if(stats[bowler] === undefined){
                stats[bowler] = { };
                if(stats[bowler][batsman] === undefined){
                    stats[bowler][batsman] = 1;
                }
                else{
                    stats[bowler][batsman]++;
                }
            }
            else{
                if(stats[bowler][batsman] === undefined){
                    stats[bowler][batsman] = 1;
                }
                else{
                    stats[bowler][batsman]++;
                }

            };
        }
    };

    const entries = Object.entries(stats);
    const entriesArray = [];

    for(let item of entries){
        const dismissalObject = item[1];
        const bowler = item[0];

        const dismissalArray = Object.entries(dismissalObject);

        const sorted = dismissalArray.sort((a,b) => b[1] - a[1]);

        const mostDismissedBatterForBowler = sorted.slice(0,1);

        for(let dismissalStat of mostDismissedBatterForBowler){
            entriesArray.push([bowler, dismissalStat])
        };

    };

    const sortedStats = entriesArray.sort((a,b) => b[1][1] - a[1][1]);

    const topStatBowler = sortedStats.slice(0,1);

    const result = Object.fromEntries(topStatBowler);

    return result;
};

function batterStrikeRatePerEachSeason(deliveries, matches, playerName) {

    const result = { };

    playerName = playerName.toLowerCase();

    for(let ball of deliveries){
        const matchIdInDeliveries = ball['match_id'];

        for(let match of matches){
            const matchIdInMatches = match['id'];
            const year = match['season'];

            if(matchIdInMatches == matchIdInDeliveries){
                ball['year'] = year;
            };
         };
    };
    

    for(let ball of deliveries){
        
        const batter = ball['batsman'].toLowerCase();
        const runs = ball['batsman_runs'];
        const year = ball['year'];

        if(batter === playerName){
            if(result[year] === undefined){

                result[year] = { };

                result[year][batter] = { 'balls' : 1,
                                        'runs' : 0 };

                result[year][batter]['runs'] = parseInt(runs);
            }
            else{
                result[year][batter]['balls']++;

                result[year][batter]['runs'] += parseInt(runs)
            };
        };
    };

    for(let item in result){

        result[item][playerName]['strikeRate'] = (result[item][playerName]['runs'] * 100)/ result[item][playerName]['balls'] ;
    }

    return result;
};


module.exports.matchesPerYear = matchesPerYear;

module.exports.matchesWonByTeamPerYear = matchesWonByTeamPerYear;

module.exports.extraRunsIn2016 = extraRunsIn2016;

module.exports.bestEconomyIn2015 = bestEconomyIn2015;

module.exports.teamsWonBothTossAndMatch = teamsWonBothTossAndMatch;

module.exports.mostManOfTheMatchAwardsPerSeason = mostManOfTheMatchAwardsPerSeason;

module.exports.bestEconomicalBowlerInSuperOvers = bestEconomicalBowlerInSuperOvers;

module.exports.batsmanDismissedMostlyByBowler = batsmanDismissedMostlyByBowler;

module.exports.batterStrikeRatePerEachSeason = batterStrikeRatePerEachSeason;